function greet(name) {
	console.log(`Hello ${name}`)
}

// function greetAli(greetFun) {
// 	const name = "Ali"
// 	greetFun(name)
// }

// greetAli(greet)

function higherOrderFunction(callback) {
	const name = "Ali"
	callback(name)
}

higherOrderFunction(greet)
