const EventEmitter = require("node:events")

const emitter = new EventEmitter()

// registering event
emitter.on("order-pizza", (size, topping) => {
	console.log(`Order placed! Baking a ${size} pizza with ${topping}`)
})

emitter.on("order-pizza", (size) => {
	if (size === "large") {
		console.log("Serving complimentary drink")
	}
})

console.log("Do work before event occurs in the system")

// emitting event
emitter.emit("order-pizza", "large", "mushroom")
