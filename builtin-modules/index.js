const path = require("node:path")

// console.log(__filename)
// console.log(__dirname)

// console.log(path.basename(__filename)) // last name of the file (index.js)
// console.log(path.basename(__dirname)) // destination dir (builtin-modules)

// console.log(path.extname(__filename)) // .js
// console.log(path.extname(__dirname)) // ""

// console.log(path.parse(__filename)) // more details about the file including (root, dir, base, ext, name)

// console.log(path.format(path.parse(__filename))) // format the details to complete path

// console.log(path.isAbsolute(__filename)) // true (if the path is exist)

console.log(path.join("dir1", "dir2", "index.html"))
console.log(path.join("/dir1", "dir2", "index.html"))
console.log(path.join("/dir1", "//dir2", "index.html"))
console.log(path.join("/dir1", "dir2", "../index.html"))
console.log(path.join(__dirname, "data.json"))

console.log(path.resolve("dir1", "dir2", "index.html"))
console.log(path.resolve("/dir1", "dir2", "index.html"))
console.log(path.resolve("/dir1", "//dir2", "index.html"))
console.log(path.resolve("/dir1", "dir2", "../index.html"))
console.log(path.resolve(__dirname, "data.json"))
