// =================== USING callback ===============

// const fs = require("node:fs")

// console.log("First")
// const fileContent = fs.readFileSync("./hello.txt", "utf-8")
// console.log(fileContent)

// console.log("Second")

// fs.readFile("./hello.txt", "utf-8", (error, data) => {
// 	if (error) {
// 		console.log(error)
// 	} else {
// 		console.log(data)
// 	}
// })

// console.log("Third")

// fs.writeFileSync("greet.txt", "Hello World!")

// /**
//  * flag set as `a` => means append (not override)
//  */
// fs.writeFile("./greet.txt", " Hello Mohammed", { flag: "a" }, (error) => {
// 	if (error) {
// 		console.log(error)
// 	} else {
// 		console.log("File written")
// 	}
// })

// =================== USING try/catch ===============

const fs = require("node:fs/promises")

// fs.readFile("./hello.txt", "utf-8")
// 	.then((data) => console.log(data))
// 	.catch((error) => console.log(error))

// =================== USING async await ===============

async function readFile() {
	try {
		const data = await fs.readFile("./hello.txt", "utf-8")
		console.log(data)
	} catch (error) {
		console.log(error)
	}
}

readFile()
