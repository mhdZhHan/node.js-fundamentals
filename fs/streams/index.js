const fs = require("node:fs")

/**
 * creating a readableStream for reading data's as chunks from a file
 */

const readableStream = fs.createReadStream("./file1.txt", {
	encoding: "utf-8",
	highWaterMark: 2,
})

/**
 * creating a writableStream for writing chunks of data to a file
 */

const writableStream = fs.createWriteStream("./file2.txt")

/**
 * setting event for retrieving chunks of data
 * and write to another file
 */

readableStream.on("data", (chunk) => {
	console.log(chunk)
	writableStream.write(chunk)
})
