const fs = require("node:fs")
const zlib = require("node:zlib")

const gzip = zlib.createGzip()

/**
 * creating a readableStream for reading data's as chunks from a file
 */

const readableStream = fs.createReadStream("./file1.txt", {
	encoding: "utf-8",
	highWaterMark: 2,
})

readableStream.pipe(gzip).pipe(fs.WriteStream("file1.text.gz"))

/**
 * creating a writableStream for writing chunks of data to a file
 */

const writableStream = fs.createWriteStream("./file2.txt")

/**
 * read chunks of data from `readableStream`
 * and write to writableStream
 * (like water is passing from tank to tap in the help of a pipe)
 */
readableStream.pipe(writableStream)
