const buffer = new Buffer.from("Mohammed")

console.log(buffer.toJSON())
console.log(buffer.toString())
console.log(buffer)

// override buffer (because it has smaller memory)
buffer.write("Hello")
console.log(buffer.toJSON())
