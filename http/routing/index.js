const http = require("node:http")

const server = http.createServer((req, res) => {
	if (req.url === "/") {
		res.writeHead(200, { "Content-Type": "text/plain" })
		res.end("Home page")
	} else if (req.url === "/about") {
		res.writeHead(200, { "Content-Type": "text/plain" })
		res.end("about page")
	} else if (req.url === "/api") {
		res.writeHead(200, { "Content-Type": "application/json" })
		res.end(
			JSON.stringify({
				id: 12765431,
				title: "Hello world blog post!",
				body: "hello world!",
			})
		)
	} else {
		res.writeHead(400, { "Content-Type": "text/plain" })
		res.end("Page not found")
	}
})

server.listen(3000, () => console.log("server is running on port 3000"))
