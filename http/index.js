const http = require("node:http")
const fs = require("node:fs")

const server = http.createServer((req, res) => {
	const blogPost = {
		id: 12323,
		title: "Hello, World!",
		body: "Hello world blog post!",
	}

	// res.writeHead(200, { "Content-Type": "text/plain" })
	// res.writeHead(200, { "Content-Type": "application/json" })
	res.writeHead(200, { "Content-Type": "text/html" })
	// res.end("hello, World!")
	// res.end(JSON.stringify(blogPost))
	// res.end("<h1>hello world!</h1>")

	// fs.createReadStream(__dirname + "/index.html").pipe(res)

	const name = "Mohammed"

	let html = fs.readFileSync("./index.html", "utf-8")
	html = html.replace("{{name}}", name)
	res.end(html)
})

server.listen(3000, () => console.log(`Server is running on port 3000`))
