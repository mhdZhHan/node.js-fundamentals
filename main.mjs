import math from "./math-jsm.mjs"

const { add, subtract } = math

console.log(add(10, 20))

console.log(subtract(30, 20))
